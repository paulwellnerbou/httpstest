package de.wellnerbou;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectionPoolTimeoutException;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.PoolingClientConnectionManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App
{
    private static final Logger LOG = LoggerFactory.getLogger(App.class);

    public static void main( String[] args )
    {
        final String url = "https://stellenmarkt.faz.net/rss/";
        final String res = new App().execute(url);

        System.out.println(res);
    }

    public DefaultHttpClient createHttpClient() {
        final HttpParams httpParams = new BasicHttpParams();

        final int connectionTimeout = 200;

        HttpConnectionParams.setConnectionTimeout(httpParams, connectionTimeout);
        HttpConnectionParams.setSoTimeout(httpParams, connectionTimeout);

        PoolingClientConnectionManager cm = new PoolingClientConnectionManager();

        // Create an HttpClient with the ThreadSafeClientConnManager.
        // This connection manager must be used if more than one thread will
        // be using the HttpClient.
        return new DefaultHttpClient(cm, httpParams);
    }

    public String execute(final String url) {
        String responseString = "";
        final HttpGet httpget = new HttpGet(url);

        try {
            final HttpClient httpClient = createHttpClient();
            final HttpResponse response = httpClient.execute(httpget);
            final HttpEntity entity = response.getEntity();

            if ((entity != null) && (response.getStatusLine().getStatusCode() == 200)) {
                final ContentType contentType = ContentType.get(entity);
                // if charset is not set we use UTF-8 as default and not ISO-8859-1
                responseString = (contentType != null && (contentType.getCharset() != null)) ? EntityUtils.toString(entity) : EntityUtils.toString(entity, "UTF-8");
            }
        } catch (final ConnectionPoolTimeoutException e) {
            LOG.error("{}: No connection from connection manager is available: {}", e.getClass().getName(), e.getMessage());
            httpget.abort();
        } catch (final Exception e) {
            LOG.error("{}: executing request {}: {}", e.getClass().getName(), url, e.getMessage());
            httpget.abort();
        }

        return responseString;
    }
}
