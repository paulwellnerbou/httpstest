package de.wellnerbou;

import org.assertj.core.api.Assertions;
import org.junit.Test;

public class AppTest {

	@Test
	public void execute() throws Exception {
		final String url = "https://stellenmarkt.faz.net/rss/";
		final String res = new App().execute(url);

		Assertions.assertThat(res).isNotEmpty();
		Assertions.assertThat(res).startsWith("<rss");
	}

}
